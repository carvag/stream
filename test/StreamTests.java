import com.gustavo.stream.*;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class StreamTests {
    
    public static char firstChar(StreamImpl streamImpl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
  @Test
  public void firstCharTests() throws Exception {


    //Input de Teste
    String input = "aAbBABacafe";

    //Resultado Esperado
    char expResult = 'e';

    //Execução do Método
    char result = StreamTests.firstChar(new StreamImpl(input));

    //O Método deve retornar o expResult
    assertEquals(expResult, result);
  }


  /**
   * Teste do método firstChar, não encontrará a vogal, retornará um Exception 
   * @throws java.lang.Exception
   */
  @Test(expected = Exception.class)
  public void charNotFoundTest() throws Exception{
   
    //Input de Teste
    String input = "aafbUuzyuU";

    //Execução do Método (Deve retornar a Exception)
    StreamTests.firstChar(new StreamImpl(input));
  }
}
