# Stream

Códigos desenvolvidos para solucionar problema proposto no exercício 3

Instruções específicas:
## 
Construir projeto e executar.

## Testes
```sh
public void firstCharTests() 
```
```sh
public void charNotFoundTest() 
```

## Stream iniciada
```sh
aAbBABacafe
```
## Saída
```sh
Vogal Encontrada: e
```

## Tecnologias Usadas

 - Java versão 8.
 - JUnit


----------
## Questão 4 - Deadlock
Deadlock traduzido ao pé da letra é um impasse, é quando ocorre uma espera por um evento que nunca ocorrerá, esta situação ocorre quando vários processos compartilham vários recursos de forma exclusiva e acaba resultando no travamento da ação que está sendo executada.
Para acontecer o Deadlock é necessário que pelo menos 4 situações ocorram ao mesmo tempo:
Utilizar a exclusão mútua;
Processo que utiliza um recurso espera por outro;
Não-preemptivo (o recurso não pode ser liberado para outro processo);
Espera circular.
Uma boa prática para seria o uso de escalonadores para verificar tempo de resposta de um processo em execução, o tempo de persistência e espera também. Ter um gerenciamento do processo em si para não ocorrer travamento em alguma ponta.


----------

##Questão 5 - API Stream e ParallelalStream
A API Stream e ParallelalSream é uma forma de programar diferente abstraindo várias partes do modo tradicional, trazendo um modelo de programação declarativa, isto é deixando que a API realize o trabalho de como percorrer uma lista por exemplo, e se importar apenas com o objetivo do problema, por exemplo no exercício 1 onde é uma soma recursiva. Tudo isso com muita eficiência. A API conta com operações de agregação para filtrar, modificar, transformar um elemento, de iteração e de listagem.
A ParallelalSream possibilita realizar as tarefas de forma concorrente e paraleliza esse processamento. É recomendado o uso paralelo da stream quando está num ambiente com vários processadores, onde tenha threads sendo executada seria interessante o uso, a não ser que tenha outros destes rodando e quando tenha muitos objetos a serem processados.

----------


License
----
GNU PUBLIC LICENSE


----------


**Gustavo Carvalho**
