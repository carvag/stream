package com.gustavo.stream;

public interface Stream {
 
   public char getNext();
   
   public boolean hasNext(); 
   
}
