package com.gustavo.stream;

public class StreamImpl implements Stream{
    
    private final String stream;
    private int idx;
    
    @Override
    public char getNext() {
        return stream.charAt(idx++);
    }

    @Override
    public boolean hasNext() {
        return stream != null && idx<stream.length();
    }

    public StreamImpl(String stream) {
        idx = 0;
        this.stream = stream;
    }
}
