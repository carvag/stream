package com.gustavo.stream.app;

import com.gustavo.stream.Stream;
import com.gustavo.stream.StreamImpl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainApp {
    
    public static void main(String[] args) {
        try {
            System.out.println("Vogal Encontrada: " + firstChar(new StreamImpl("aAbBABacafe")));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    /***
    * Localiza a primeira vogal unica em uma Stream seguida por uma vogal e uma consoante
    * 
    * @param input
    * @return Vogal encontrada
    * @throws Exception Retorna exceção caso não encontre o padrão
    */
    public static char firstChar(Stream input) throws Exception {
        
        /*
        Padrão para teste de ocorrencia (vogalconsoante)(vogal)
        */
        Pattern pattern = Pattern.compile("(?i)([aeiou][a-z-[aeiou]])([aeiou])");
        String regexIsVogal = "(?i)[aeiou]";
        String bufferTest = "";

        
        /*
        Hash para incluir as vogais encontradas dos possíveis retornos
        */
        List<Character> retorno;
        retorno = new ArrayList<>();


        /*
        Hash para incluir as vogais encontradas na iteração do stream
        */
        HashSet<Character> vogaisEncontradas;
        vogaisEncontradas = new HashSet<>();
        
        /*
        Iteração do Stream
        */
        while(input.hasNext()){
            Character aux = input.getNext();
            
            bufferTest += aux.toString();

            /*
            Buffer para que o teste seja feito apenas com as últimas três posicoes do Stream, otimizando memória
            */
            if(bufferTest.length() > 3)
                bufferTest = bufferTest.substring(1);
            
                  
            /*
            Testa o buffer se encontrou o padrão (vogalconsolante)(vogal)
            Se encontrou o padrão verifica se a vogal não está no Hash de Vogais Econtradas.
            Se encontrou o padrão e a vogal não está no Hash de vogais encontradas a rotina encontrou um possível retorno.
            */
            Matcher matcher = pattern.matcher(bufferTest);
            if(matcher.find() 
                    && !vogaisEncontradas.contains(matcher.group(2).charAt(0)))
                retorno.add(aux);
            
            /*
            Condição para checar até o final da Strem, se o possível retorno encontrado (Upper e Lower Case) não ocorre no decorrer da iteração da Stream
            */          
            if(aux.toString().matches(regexIsVogal))
                if(!vogaisEncontradas.add(Character.toLowerCase(aux)) 
                        && (retorno.contains(Character.toLowerCase(aux))||retorno.contains(Character.toUpperCase(aux)))){
                    retorno.remove(Character.toLowerCase(aux));retorno.remove(Character.toUpperCase(aux));
                }
            
        }     
        /*
        Retorna uma exceção caso não encontre o padrão
        */
        if(retorno.isEmpty()) throw new Exception("Vogal não encontrada");
        
        return retorno.get(0);
    } 
    
}
